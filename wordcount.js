/**
 * Credit: https://amystechnotes.com/2015/05/06/tinymce-add-character-count/
 * This is a slightly modified version to work with more recent TinyMCE version, fix some code styling and don't trim
 * trailing and leading whitespaces from count.
 */

(function() {
  tinymce.PluginManager.add('charactercount', function(editor) {
    var _self = this;

    function update(event) {
      var ignoreKeys = [
        38, // up
        40, // down
        37, // left
        39, // right
        13, // enter
        8, // backspace
        27, // ESC
        16, // Uppercase
      ];
      editor.theme.panel.find('#charactercount').text(['Characters: {0}', _self.getCount()]);
      var num = _self.getCount();
      let el = editor.theme.panel.find('#charactercount')[0].$el;
      var label = el.parents('.form-group').find('label').text();
      var max = label.match(/max\. (\d+)/);
      if(max) {
        maxLen = Number(max[1]);
        if(num > maxLen) {
          // Mark character count
          jQuery(el).addClass('mark-red');
          /**
           if( ignoreKeys.indexOf(event.keyCode) === -1 && event.keyCode !== undefined) {
            // Slice content and mark overflow content red
            // if used "text", wordcount is correct but looses formatting
            let content = editor.getContent({format: 'raw'});

            let a = content.slice(0, maxLen);
            let b = content.slice(maxLen, content.length);
            editor.setContent(a + '<span class="mark-red">' + b + '</span>', {format: 'raw'});
            // Put cursor at the end of the content
            // @see https://www.phpzag.com/insert-content-at-cursor-position-in-tinymce-rich-text-editor/
            tinyMCE.activeEditor.selection.select(tinyMCE.activeEditor.getBody(), true);
            tinyMCE.activeEditor.selection.collapse(false);
          }**/
        } else {
          jQuery(el).removeClass('mark-red');
        }
      }
    }

    editor.on('init', function() {
      var statusbar = editor.theme.panel && editor.theme.panel.find('#statusbar')[0];
      if(statusbar) {
        window.setTimeout(function() {
          statusbar.insert({
            type: 'label',
            name: 'charactercount',
            text: ['Characters: {0}', _self.getCount()],
            classes: 'charactercount',
            disabled: editor.settings.readonly
          }, 0);
          // If used with "setcontent", the update triggers an endlessloop
          // editor.on('setcontent beforeaddundo keyup', update);
          editor.on('beforeaddundo keyup', update);
        }, 0);
      }
    });
    _self.getCount = function() {
      var tx = editor.getContent({format: 'raw'});
      var decoded = decodeHtml(tx);
      var decodedStripped = decoded.replace(/(<([^>]+)>)/ig, '');
      var tc = decodedStripped.length;
      return tc;
    };

    function decodeHtml(html) {
      var txt = document.createElement('textarea');
      txt.innerHTML = html;
      return txt.value;
    }
  });
})();