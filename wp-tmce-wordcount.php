<?php
/**
 * Plugin Name: Raskin TinyMCE Character Counter
 * Description: Counts Characters and marks overflow text red. requires string (max. xxx) in label.
 * Version: 0.3
 * Last Update: 2019-04-04
 * Author: Jam
 * Author URI: https://hashcookie.ch
 **/

class TinyMCE_Wordcount
{
	function __construct()
	{
		add_action('init', array(&$this, 'setup_tinymce_plugin'));
		wp_enqueue_style('wordcount', plugins_url('wordcount.css', __FILE__));
	}

	/**
	 * Call wordpress filter to change tinyMCE settings
	 */
	function setup_tinymce_plugin()
	{
		add_filter('mce_external_plugins', array(&$this, 'add_tinymce_plugin'));
		add_filter('tiny_mce_before_init', array(&$this, 'my_format_TinyMCE'));
	}

	/**
	 * Update tinyMCE config to have content styles
	 *
	 * @param $in
	 *
	 * @return mixed
	 */
	function my_format_TinyMCE($in)
	{
		/**
		 * $in['remove_linebreaks'] = false;
		 * $in['gecko_spellcheck'] = false;
		 * $in['keep_styles'] = true;
		 * $in['accessibility_focus'] = true;
		 * $in['tabfocus_elements'] = 'major-publishing-actions';
		 * $in['media_strict'] = false;
		 * $in['paste_remove_styles'] = false;
		 * $in['paste_remove_spans'] = false;
		 * $in['paste_strip_class_attributes'] = 'none';
		 * $in['paste_text_use_dialog'] = true;
		 * $in['wpeditimage_disable_captions'] = true;
		 * $in['plugins'] = 'tabfocus,paste,media,wordpress,wpeditimage,wpgallery,wplink,wpdialogs';
		 * $in['content_css'] = get_template_directory_uri() . "/editor-style.css";
		 * $in['wpautop'] = true;
		 * $in['apply_source_formatting'] = false;
		 * $in['block_formats'] = "Paragraph=p; Heading 3=h3; Heading 4=h4";
		 * $in['toolbar1'] = 'bold,italic,strikethrough,bullist,numlist,blockquote,hr,alignleft,aligncenter,alignright,link,unlink,wp_more,spellchecker,wp_fullscreen,wp_adv ';
		 * $in['toolbar2'] = 'formatselect,underline,alignjustify,forecolor,pastetext,removeformat,charmap,outdent,indent,undo,redo,wp_help ';
		 * $in['toolbar3'] = '';
		 * $in['toolbar4'] = '';
		 * */
		$in['content_style'] = "span.mark-red {color:red !important}";

		return $in;
	}

	/**
	 * Adds a TinyMCE plugin compatible JS file to the TinyMCE / Visual Editor instance
	 *
	 * @param array $plugin_array Array of registered TinyMCE Plugins
	 *
	 * @return array Modified array of registered TinyMCE Plugins
	 */
	function add_tinymce_plugin($plugin_array)
	{
		$plugin_array['charactercount'] = plugin_dir_url(__FILE__) . 'wordcount.js';

		return $plugin_array;
	}

}

new TinyMCE_Wordcount;